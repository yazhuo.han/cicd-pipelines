def incVersion() {
    echo "incrementing app version..."
    sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}


def buildApp() {
    echo "building the application..."
    sh 'mvn clean package'
    
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'ecr-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_REPO}:${IMAGE_NAME} ."
        sh "echo $PASS | docker login -u $USER --password-stdin ${ECR_REPO_URL}"
        sh "docker push ${IMAGE_REPO}:${IMAGE_NAME}"

    }
} 

def deployApp() {
    echo 'deploying java-maven-app docker image...'
    sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
    sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'

// create secret locally to store credentials for k8s pulling image from ecr
// $ kubectl create secret docker registry aws-registry-key --docker-server=419750560343.dkr.ecr.us-east-1.amazonaws.com --docker-username=AWS \
// > --docker-password=xxx

} 

def versionGitUpdate() {
    echo "keep the updated version in GitLab"
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/yazhuo.han/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:cicd/jenkins-ecr-to-eks'
    }
}


return this