Create a kubeconfig file to connect to an EKS cluster in a Jenkins Docker container,
the actual steps I did, which is different than Nana's. The curl url in the course video is deprecated.

1. Install AWS CLI
$ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
$ unzip awscliv2.zip
$ sudo ./aws/install

2. Install kubectl
$ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
$ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

3. Use AWS configure
$ aws configure
this is to set credentials to my aws account

4. Use the AWS CLI to create the kubeconfig file by running the following command:
$ aws eks update-kubeconfig --name <cluster-name> --region <region> --kubeconfig <kubeconfig-file>
 Replace `<cluster-name>` with the name of your EKS cluster, `<region>` with the AWS region where the cluster is located, and `<kubeconfig-file>` with the path and filename for your kubeconfig file (e.g., `/path/to/kubeconfig`).

 5. Set the `KUBECONFIG` environment variable to the path of your kubeconfig file:
 $ export KUBECONFIG=/path/to/kubeconfi

 This ensures that kubectl uses the correct configuration file to connect to EKS cluster.
check the connection:
$ kubectl get nodes (I already created a cluster that has two worker nodes in it)

