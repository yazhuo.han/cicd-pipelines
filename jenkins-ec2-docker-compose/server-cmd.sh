#!/usr/bin/env bash

export IMAGE=$1

# //docker-compose -f docker-compose.yaml pull
docker-compose -f docker-compose.yaml up --detach

echo "shell success"

# IMAGE, from Jenkinsfile to Shell Script via Parameter
# this environmental variable will be set on ec2 server
# then, access environment variable in docker-compose file

# docker-compose pull
# will first pull the latest version of the image from Docker Hub and then start the containers in detached mode