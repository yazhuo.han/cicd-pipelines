def incVersion() {
    echo "incrementing app version..."
    sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]// [0] is the version [1] is the tag
    // version represents the application version, version value reads in between <version> </version> from pom.xml
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
    // append jenkins build number to version number
    //BUILD_NUMBER is an environmental variable that Jenkins makes available in our
    // pipeline job, we are appending it as a suffix to a version
    // in a word, this line of code gives us a new Docker Image name/tag
    // IMAGE_NAME = new Image name
}

def buildJar() {
    echo "building the application..."
    sh 'mvn clean package' 
    // mvn package = build jar file with version from pom.xml
    // mvn clean package = clean the target folder from previous build
} 

def buildImage() {
    //use application version for Docker Image Version.
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t codingtruckingjoe/my-repo:${IMAGE_NAME} ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push codingtruckingjoe/my-repo:${IMAGE_NAME}"
    }
} 
// docker build command takes a Dockerfile and build a docker image based on it.
 


def deployApp() {
    
    echo 'deploying the application...'
// in order to execute docker-compose command, we need docker-compose.yaml file on EC2 instance!
// $ docker-compose -f docker-compose.yaml up
// --detach flag is important, otherwise, our build will run infinitely

// *** Improvement: extract to Shell Script *** def dockerComposeCmd = "docker-compose -f docker-compose.yaml up --detach"

    // def shellCmd = "bash ./server-cmd.sh ${IMAGE_NAME}" // this means that my shell script also needs to be on ec2 like .yaml file
    
    def ec2Instance = "ec2-user@52.23.206.139"
    def dockerComposeCmd = "docker-compose -f docker-compose.yaml up --detach"

    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
        sshagent(['ec2-server-key']) {
            sh "scp server-cmd.sh ${ec2Instance}:/home/ec2-user"
            sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
            sh "echo $PASS | docker login -u $USER --password-stdin"
            sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${dockerComposeCmd}"
            // sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
        }
    }
   
}



def versionGitUpdate() {
    echo "keep the updated version in GitLab"
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        // sh 'git config --global user.email "jenkins@jenkins.com"'
        // sh 'git config --global user.name "jenkins"'

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/yazhuo.han/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:cicd/jenkins-ec2-docker-compose'
    }
}

return this