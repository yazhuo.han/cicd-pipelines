## Steps of the Project: 

1. Create ssh key-pair
2. Install Terraform inside Jenkins container
3. Create Terraform configuration files to provision server
4. Adjust Jenkinsfile

## 1. Create ssh key-pair
On aws, create a key pair. Then add the credential to Jenkins. (kind: SSH username with private key)

## 2. Install Terraform inside Jenkins container.
ssh into droplet
$ docker ps

$ docker exec -it -u 0 <container-id> bash

### checking the os of the container
```
cat /etc/os-release 
```
### add HashiCorp key
```
curl -fsSL https://atp.releases.hashicorp.com/gpg | apt-key add - 
```
### install apt-add-repo command   
```
apt-get install software-properties-common 
```
### add the official HashiCorp Linux repository
```
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
```
### install Terraform
```
apt-get update 
```
```
apt-get install terraform
```
```
terraform -v
```


