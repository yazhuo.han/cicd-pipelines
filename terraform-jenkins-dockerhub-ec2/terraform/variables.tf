variable vpc_cidr_block {
    default = "10.0.0.0/16"
}
variable subnet_cidr_block {
    default = "10.0.10.0/24"
}
variable avail_zone {
    default = "us-east-2a"
}
variable env_prefix {
    default = "prod"
}
variable my_ip {
    default = "75.8.230.66/32"
}
variable jenkins_ip {
    default = "157.230.189.213/32"
}
variable instance_type {
    default = "t2.micro"
}
variable region {
    default = "us-east-2"
}