def incVersion() {
    echo "incrementing app version..."
    sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}


def buildApp() {
    echo "building the application..."
    sh 'mvn clean package'
    
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_REPO}:${IMAGE_NAME} ."
        sh "echo $PASS | docker login -u $USER --password-stdin" // docker login on Jenkins server
        sh "docker push ${IMAGE_REPO}:${IMAGE_NAME}"
    }
} 

def provServer() {
    dir('terraform') {
        sh "terraform init"
        sh "terraform apply --auto-approve"
        EC2_PUBLIC_IP = sh(
            script: "terraform output ec2_public_ip", //print out the value to standard output so that we can save it into a variable
            returnStdout: true
        ).trim()
    }
}


def deployApp() {
    echo "waiting for EC2 server to initialize" 
    sleep(time: 90, unit: "SECONDS") // give aws ec2 some time to initialize

    echo 'deploying docker image to EC2...'
    echo "${EC2_PUBLIC_IP}"
    // docker login on ec2 server
    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"
    def ec2Instance = "ec2-user@${EC2_PUBLIC_IP}"

    sshagent(['server-ssh-key']) {
        sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
    }
} 

def versionGitUpdate() {
    echo "keep the updated version in GitLab"
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/yazhuo.han/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:cicd/terraform-jenkins-dockerhub-ec2'
    }
}


return this
