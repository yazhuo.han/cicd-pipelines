def incVersion() {
    echo "incrementing app version..."
    sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}


def buildApp() {
    echo "building the application..."
    sh 'mvn clean package'
    
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_REPO}:${IMAGE_NAME} ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push ${IMAGE_REPO}:${IMAGE_NAME}"
    }
} 

def deployApp() {
    echo 'deploying java-maven-app docker image...'
    sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
    sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'
    // envsubst does not come out of the box, we need to install "gettext-base" tool in Jenkins container"
    // into the Jenkins container, apt-get update, apt-get install gettext-base
} 

def versionGitUpdate() {
    echo "keep the updated version in GitLab"
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/yazhuo.han/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:cicd/jenkins-dockerhub-to-eks'
    }
}


return this
